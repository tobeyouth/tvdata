import createFetchTypes  from 'utils/create-fetch-types'

// 地区用户排名
export const RegionDataFetch = createFetchTypes('region-data', 'fetch')

// 实时在线人数/今日新增人数
export const ActiveDataFetch = createFetchTypes('active-data', 'fetch')

// 电影热播
export const MovieDataFetch = createFetchTypes('movie-data', 'fetch')

// 剧集热播
export const PlayDataFetch = createFetchTypes('play-data', 'fetch')

// 教育热播
export const EduDataFetch = createFetchTypes('edu-data', 'fetch')

// 应用排行
export const AppDataFetch = createFetchTypes('app-data', 'fetch')

// 分时播放 vv/分时播放用户
export const TimePlayDataFetch = createFetchTypes('play-vv-data', 'fetch')

// 分时点播 vv/分时点播用户
export const PlayUVDataFetch = createFetchTypes('play-uv-data', 'fetch')

// 用户月活跃趋势/新增用户
export const MonthActiveDataFetch = createFetchTypes('mounth-active-data', 'fetch')

// 累计新增用户接口
export const TotalUserDataFetch = createFetchTypes('total-user', 'fetch')

// 日均数据
export const AverageDataFetch = createFetchTypes('average-data', 'fetch')