const PREFIX = 'dashboard'
let EXIST_CLASSNAME = []

export default (scope, prefix=PREFIX) => {
  return (name) => {
    let classname = [prefix, scope, name].filter((item) => !!item).join('-')
    if (EXIST_CLASSNAME.indexOf(classname) > -1) {
      console.warn(`${classname} 已经存在, 换个 classname 吧`)
    }
    return classname
  }
}
