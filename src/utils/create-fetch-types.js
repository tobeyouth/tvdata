/**
 * 创建带 scope 和 status 的 fetch action types
 * example:
 * createFetchTypes('topics', 'fetch')
 * return:
 * {
 *  fetch: 'TOPICS_FETCH',
 *  success: 'TOPICS_FETCH_SUCCESS',
 *  fail: 'TOPICS_FETCH_FAIL'
 * }
 */
import { SUCCESS, FAIL } from 'constants/async-status'

export default (scope, name) => {
  if (!scope) {
    throw new Error('定义 action types 时缺少 scope')
    return null
  }

  let _default = [scope, name].map((item) => item.toUpperCase()).join('-')
  let _success = [scope, name, SUCCESS].map((item) => item.toUpperCase()).join('-')
  let _fail = [scope, name, FAIL].map((item) => item.toUpperCase()).join('-')
  return {
    [name]: _default,
    success: _success,
    fail: _fail
  }
}