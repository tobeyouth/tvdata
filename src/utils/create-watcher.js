/**
 * 创建个 sagas 里面的 watcher
 */

import * as effects from 'redux-saga/effects'

export default (type, saga, func='takeLatest') => {
  return function* (action) {
    yield effects[func](type, saga)
  }
}