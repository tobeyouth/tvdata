import React from 'react'
import ReactDOM from 'react-dom'
import App from './views/dashboard/index'

let root = document.getElementById('app')

ReactDOM.render(<App />, root)