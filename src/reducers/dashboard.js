import { DEFAULT, PENDING, SUCCESS, FAIL } from 'constants/async-status'
import { RegionDataFetch, ActiveDataFetch, MovieDataFetch,
         PlayDataFetch, EduDataFetch, AppDataFetch,
         TimePlayDataFetch, PlayUVDataFetch,
         MonthActiveDataFetch, AverageDataFetch,
         TotalUserDataFetch } from 'constants/action-types'

export const STATE = {
  regionData: null,
  regionStatus: DEFAULT,
  regionTotal: 0,

  activeData: null,
  activeStatus: DEFAULT,

  movieData: null,
  movieStatus: DEFAULT,

  playData: null,
  playStatus: DEFAULT,

  eduData: null,
  eduStatus: DEFAULT,

  appData: null,
  appStatus: DEFAULT,

  playVVData: null,
  playVVStatus: DEFAULT,
  playVVTotal: 0,

  playUVData: null,
  playUVStatus: DEFAULT,
  playUVTotal: 0,

  mounthMonthActiveData: null,
  activeStatus: DEFAULT,

  averageData: null,
  averageStatus: DEFAULT,

  totalUserData: null,
  totalUserStatus: DEFAULT
}

export default function (state=STATE, action) {

  switch (action.type) {

    // regionData
    case RegionDataFetch.fetch:
      {
        const _state = { ...state }
        _state.regionStatus = PENDING
        return _state
      }
    
    case RegionDataFetch.success:
      {
        const _state = { ...state }
        if (action.res) {
          _state.regionStatus = SUCCESS
          if (action.res.data) {
            _state.regionData = action.res.data
          }
          if (action.res.total) {
            _state.regionTotal = action.res.total
          }
        }
        return _state
      }

    case RegionDataFetch.fail:
      {
        const _state = { ...state }
        _state.regionStatus = FAIL
        return _state
      }

    // activeData
    case ActiveDataFetch.fetch:
      {
        const _state = { ...state }
        _state.activeStatus = PENDING
        return _state
      }

    case ActiveDataFetch.success:
      {
        const _state = { ...state }
        if (action.res) {
          _state.activeStatus = SUCCESS
          _state.activeData = action.res
        }
        return _state
      }

    case ActiveDataFetch.fail:
      {
        const _state = { ...state }
        _state.activeStatus = FAIL
        return _state
      }

    // movieData
    case MovieDataFetch.fetch:
      {
        const _state = { ...state }
        _state.movieStatus = PENDING
        return _state
      }

    case MovieDataFetch.success:
      {
        const _state = { ...state }
        if (action.res) {
          _state.movieStatus = SUCCESS
          _state.movieData = action.res
        }
        return _state
      }

    case MovieDataFetch.fail:
      {
        const _state = { ...state }
        _state.movieStatus = FAIL
        return _state
      }

    // playData
    case PlayDataFetch.fetch:
      {
        const _state = { ...state }
        _state.playStatus = PENDING
        return _state
      }

    case PlayDataFetch.success:
      {
        const _state = { ...state }
        if (action.res) {
          _state.playStatus = SUCCESS
          _state.playData = action.res
        }
        return _state
      }

    case PlayDataFetch.fail:
      {
        const _state = { ...state }
        _state.playStatus = FAIL
        return _state
      }

    // eduData
    case EduDataFetch.fetch:
      {
        const _state = { ...state}
        _state.eduStatus = PENDING
        return _state
      }

    case EduDataFetch.success:
      {
        const _state = { ...state}
        if (action.res) {
          _state.eduStatus = SUCCESS
          _state.eduData = action.res
        }
        return _state
      }

    case EduDataFetch.fail:
      {
        const _state = { ...state}
        _state.eduStatus = FAIL
        return _state
      }

    // appData
    case AppDataFetch.fetch:
      {
        const _state = { ...state }
        _state.appStatus = PENDING
        return _state
      }

    case AppDataFetch.success:
      {
        const _state = { ...state }
        if (action.res) {
          _state.appStatus = SUCCESS
          _state.appData = action.res
        }
        return _state
      }

    case AppDataFetch.fail:
      {
        const _state = { ...state }
        _state.appStatus = FAIL
        return _state
      }

    // playVVData
    case TimePlayDataFetch.fetch:
      {
        const _state = { ...state }
        _state.playVVStatus = PENDING
        return _state
      }

    case TimePlayDataFetch.success:
      {
        const _state = { ...state }
        if (action.res) {
          _state.playVVStatus = SUCCESS
          if (action.res.data) {
            _state.playVVData = action.res.data
          }
          if (action.res.total) {
            _state.playVVTotal = action.res.total
          }
        }
        
        return _state
      }

    case TimePlayDataFetch.fail:
      {
        const _state = { ...state }
        _state.playVVStatus = FAIL
        return _state
      }

    // playUVData
    case PlayUVDataFetch.fetch:
      {
        const _state = { ...state }
        _state.playUVStatus = PENDING
        return _state
      }

    case PlayUVDataFetch.success:
      {
        const _state = { ...state }
        if (action.res) {
          _state.playUVStatus = SUCCESS
          if (action.res.data) {
            _state.playUVData = action.res.data
          }
          if (action.res.total) {
            _state.playUVTotal = action.res.total
          }
        }
        return _state
      }

    case PlayUVDataFetch.fail:
      {
        const _state = { ...state }
        _state.playUVStatus = FAIL
        return _state
      }

    // mounthMonthActiveData
    case MonthActiveDataFetch.fetch:
      {
        const _state = { ...state }
        _state.activeStatus = PENDING
        return _state
      }

    case MonthActiveDataFetch.success:
      {
        const _state = { ...state }
        if (action.res) {
          _state.activeStatus = SUCCESS
          _state.mounthMonthActiveData = action.res
        }
        return _state
      }

    
    case MonthActiveDataFetch.fail:
      {
        const _state = { ...state }
        _state.activeStatus = FAIL
        return _state
      }

    // averageData
    case AverageDataFetch.fetch:
      {
        const _state = { ...state }
        _state.averageStatus = PENDING
        return _state
      }

    case AverageDataFetch.success:
      {
        const _state = { ...state }
        if (action.res) {
          _state.averageStatus = SUCCESS
          _state.averageData = action.res
        }
        return _state
      }

    case AverageDataFetch.fail:
      {
        const _state = { ...state }
        _state.averageStatus = FAIL
        return _state
      }

    case TotalUserDataFetch.fetch:
      {
        const _state = { ...state }
        _state.totalUserStatus = PENDING
        return _state
      }

    case TotalUserDataFetch.success:
      {
        const _state = { ...state }
        if (action.res) {
          _state.totalUserStatus = SUCCESS
          _state.totalUserData = action.res
        }
        return _state
      }
    
    case TotalUserDataFetch.fail:
      {
        const _state = { ...state }
        _state.totalUserStatus = FAIL
        return _state
      }
    
    default:
      return state
  }
}

// helper
/**
 * 传入 action.type
 * 解析出 action 的触发的数据类型
 * 返回 region, movie, play 这样的 action 类型
 */
function getActionTrigger(type) {
  return type.split('-')[0].toLowerCase()
}