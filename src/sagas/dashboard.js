import 'regenerator-runtime/runtime'
import { takeLatest, takeEvery, fork, call, put } from 'redux-saga/effects'
import { RegionDataFetch, ActiveDataFetch, MovieDataFetch,
         PlayDataFetch, EduDataFetch, AppDataFetch,
         TimePlayDataFetch, PlayUVDataFetch,
         MonthActiveDataFetch, AverageDataFetch, 
         TotalUserDataFetch } from 'constants/action-types'
import { fetchRegionData, fetchActiveData, fetchMovieData,
         fetchPlayData, fetchEduData, fetchAppData, 
         fetchTimePlayData, fetchPlayUVData,
         fetchMonthActiveData, fetchAverageData,
         fetchTotalUserData } from 'effects/dashboard'
import createWatcher from 'utils/create-watcher'


export function* regionFetchSaga (action) {
  const res = yield call(fetchRegionData)
  if (res) {
    return yield put({type: RegionDataFetch.success, res})
  } else {
    return yield put({type: RegionDataFetch.fail, error: res.messaga})
  }
}

export function* activeFetchSaga (action) {
  const res = yield call(fetchActiveData)
  if (res) {
    return yield put({type: ActiveDataFetch.success, res})
  } else {
    return yield put({type: ActiveDataFetch.fail, error: res.messaga})
  }
}

export function* movieFetchSaga (action) {
  const res = yield call(fetchMovieData)
  if (res) {
    return yield put({type: MovieDataFetch.success, res})
  } else {
    return yield put({type: MovieDataFetch.fail, error: res.messaga})
  }
}

export function* playFetchSaga (action) {
  const res = yield call(fetchPlayData)
  if (res) {
    return yield put({type: PlayDataFetch.success, res})
  } else {
    return yield put({type: PlayDataFetch.fail, error: res.messaga})
  }
}

export function* eduFetchSaga (action) {
  const res = yield call(fetchEduData)
  if (res) {
    return yield put({type: EduDataFetch.success, res})
  } else {
    return yield put({type: EduDataFetch.fail, error: res.messaga})
  }
}

export function* appFetchSaga (action) {
  const res = yield call(fetchAppData)
  if (res) {
    return yield put({type: AppDataFetch.success, res})
  } else {
    return yield put({type: AppDataFetch.fail, error: res.messaga})
  }
}

export function* playVVFetchSaga (action) {
  const res = yield call(fetchTimePlayData)
  if (res) {
    return yield put({type: TimePlayDataFetch.success, res})
  } else {
    return yield put({type: TimePlayDataFetch.fail, error: res.messaga})
  }
}

export function* playUVFetchSaga (action) {
  const res = yield call(fetchPlayUVData)
  if (res) {
    return yield put({type: PlayUVDataFetch.success, res})
  } else {
    return yield put({type: PlayUVDataFetch.fail, error: res.messaga})
  }
}


export function* monthActiveFetchSaga (action) {
  const res = yield call(fetchMonthActiveData)
  if (res) {
    return yield put({type: MonthActiveDataFetch.success, res})
  } else {
    return yield put({type: MonthActiveDataFetch.fail, error: res.messaga})
  }
}

export function* averageFetchSaga (action) {
  const res = yield call(fetchAverageData)
  if (res) {
    return yield put({type: AverageDataFetch.success, res})
  } else {
    return yield put({type: AverageDataFetch.fail, error: res.messaga})
  }
}

export function* totalUserFetchSaga(action) {
  const res = yield call(fetchTotalUserData)
  if (res) {
    return yield put({type: TotalUserDataFetch.success, res})
  } else {
    return yield put({type: TotalUserDataFetch.fail, error: res.messaga})
  }
}

export const sagas = [
  createWatcher(RegionDataFetch.fetch, regionFetchSaga),
  createWatcher(ActiveDataFetch.fetch, activeFetchSaga),
  createWatcher(MovieDataFetch.fetch, movieFetchSaga),
  createWatcher(PlayDataFetch.fetch, playFetchSaga),
  createWatcher(EduDataFetch.fetch, eduFetchSaga),
  createWatcher(AppDataFetch.fetch, appFetchSaga),
  createWatcher(TimePlayDataFetch.fetch, playVVFetchSaga),
  createWatcher(PlayUVDataFetch.fetch, playUVFetchSaga),
  createWatcher(MonthActiveDataFetch.fetch, monthActiveFetchSaga),
  createWatcher(AverageDataFetch.fetch, averageFetchSaga),
  createWatcher(TotalUserDataFetch.fetch, totalUserFetchSaga)
]

export default function* () {
  yield sagas.map((saga) => {
    return fork(saga)
  })
}