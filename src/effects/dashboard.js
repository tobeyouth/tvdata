import Fetch from 'utils/fetch'
import clearQuery from 'utils/clear-query'

let HOST = ''
if (process.env.NODE_ENV === 'production') {
  HOST = 'http://47.104.150.10:8080/ecoupon'
}

// apis
const REGION_API = '/datawall/region'
const ACTIVE_API = '/datawall/active'
const MOVIE_API = '/datawall/movie'
const PLAY_API = '/datawall/teleplay'
const EDU_API = '/datawall/education'
const APP_API = '/datawall/app'
const PLAY_VV_API = '/datawall/playvv'
const PLAY_UV_API = '/datawall/playuv'
const MONTH_ACTIVE_API = '/datawall/monthactive'
const AVERAGE_API = '/datawall/average'
const TOTAL_USER_API = '/datawall/totaluser'

export function fetchRegionData () {
  return apiFetch(REGION_API)
}

export function fetchActiveData () {
  return apiFetch(ACTIVE_API)
}

export function fetchMovieData () {
  return apiFetch(MOVIE_API)
}

export function fetchPlayData () {
  return apiFetch(PLAY_API)
}

export function fetchEduData () {
  return apiFetch(EDU_API)
}

export function fetchAppData () {
  return apiFetch(APP_API)
}

export function fetchTimePlayData () {
  return apiFetch(PLAY_VV_API)
}

export function fetchPlayUVData () {
  return apiFetch(PLAY_UV_API)
}

export function fetchMonthActiveData () {
  return apiFetch(MONTH_ACTIVE_API)
}


export function fetchAverageData () {
  return apiFetch(AVERAGE_API)
}

export function fetchTotalUserData() {
  return apiFetch(TOTAL_USER_API)
}





// helper
function apiFetch(api) {
  return Fetch.get(`${HOST}${api}`)
              .then((res) => { return res })
              .catch((err) => { return err })
}