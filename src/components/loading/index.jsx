import './style.scss'
import React from 'react'

let DEFAULT_PROPS = {
  size: 120
}
export default function (props) {
  let _props = Object.assign({}, DEFAULT_PROPS, props)
  let style = {
    width: `${_props.size}px`,
    height: `${_props.size}px`,
    fontSize: `${_props.size}px`
  }
  return (
    <div className='loading-wrap'>
      <i className="icon loading" style={ style }></i>
    </div>
  )
}