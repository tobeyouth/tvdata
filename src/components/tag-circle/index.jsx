import './style.scss'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import clsGen from 'utils/classname-generator'

const clsCreator = clsGen('tag-circle')
const ANIMATE_TIMER = 500

class TagCircle extends Component {

  static propTypes = {
    title: PropTypes.string,
    tags: PropTypes.array
  }

  static defaultProps = {
    title: '热门榜单',
    tags: []
  }

  constructor (props) {
    super(props)
    this.state = {
      current: 0
    }
  }

  animationStart = () => {
    let { tags } = this.props
    if (!tags || tags.length < 2) {
      return null
    }

    let tagsLen = tags.length
    this.animateTimer = setInterval(() => {
      let { current } = this.state
      let next = current + 1
      if (next === tagsLen) {
        next = 0
      }
      this.setState({
        current: next
      })
    }, ANIMATE_TIMER)
  }

  animationStop = () => {
    clearInterval(this.animateTimer)
  }

  componentDidMount () {
    this.animationStart()
  }

  componentWillUnmount () {
    this.animationStop()
  }

  render () {
    const { title, tags } = this.props
    return (
      <div className={ clsCreator() }>
        <div className={ clsCreator('title') }>
          { title }
        </div>
        <ul className={ clsCreator('list') }>
          {
            tags.map((tag, index) => {
              return (
                <li key={`_tag_${index}`}>
                  { tag }
                </li>
              )
            })
          }
        </ul>
      </div>
    )
  }
}

export default TagCircle