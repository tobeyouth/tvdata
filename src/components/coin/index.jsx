import './style.scss'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import clsGen from 'utils/classname-generator'


const clsCreator = clsGen('coin', '')

class Coin extends Component {

  static propTypes = {
    front: PropTypes.node,
    back: PropTypes.node,
    side: PropTypes.oneOf(['front', 'back']),
    size: PropTypes.number
  }

  static defaultProps = {
    side: 'front'
  }

  render () {
    const { front, back, side, size } = this.props
    let style = {
      width: `${size}px`,
      height: `${size}px`
    }
    return (
      <div className={ classnames(clsCreator(), side) } style={ style }>
        <div className={ clsCreator('flipper') } style={ style }>
          <div className={ clsCreator('front') }>
            { front }
          </div>
          <div className={ clsCreator('back') }>
            { back }
          </div>
        </div>
      </div>
    )
  }

}

export default Coin