import React, { Component } from 'react'
import { Provider } from 'react-redux'
import configureStore from 'stores/configure-store'
import DasboardView from './view'
import DasboardReducer from 'reducers/dashboard'
import DashboardWatcher from 'sagas/dashboard'


let store = configureStore(DasboardReducer, DashboardWatcher)

class Dashboard extends Component {
  render () {
    return (
      <Provider store={ store }>
        <DasboardView { ...this.props } />
      </Provider>
    )
  }
}

export default Dashboard