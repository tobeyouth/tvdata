import './style.scss'
import GeoJson from 'constants/china.geo.json'

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { PENDING, SUCCESS, FAIL, DEFAULT } from 'constants/async-status'
import * as DashboardActions from 'actions/dashboard'
import clsGen from 'utils/classname-generator'
import moment from 'moment'
import { Chart, Legend, Axis, Tooltip, Geom, Coord, Label, View } from 'bizcharts'
import DataSet, { DataView } from '@antv/data-set'
import classnames from 'classnames'
import Loading from 'components/loading'
import Coin from 'components/coin'
import TagCircle from 'components/tag-circle'
import { getQuery } from 'utils/url-parse'

const clsCreator = clsGen()
const CHART_COLORS = ['#ce603e', '#3deefa']
const DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss'
const PERCENT_COLOR = ['#127A0B', '#999400', '#ECC6FA']
const CHART_TEXT = '#3EE6E7'
const CHART_LINE = '#00A2EE'
const COIN_ROLL_TIMER = 30000
const REFRESH_TIMER = getQuery(window.location.href, 'refresh') || 30

class DashboardView extends Component {

  startCoinRoll = () => {
    setInterval(() => {
      let { coinSide } = this.state
      if (coinSide === 'front') {
        this.setState({
          coinSide: 'back'
        })
      } else if (coinSide === 'back') {
        this.setState({
          coinSide: 'front'
        })
      }
    }, COIN_ROLL_TIMER)
  }

  constructor (props) {
    super(props)
    this.state = {
      coinSide: 'front'
    }
  }

  fetchData = () => {
    const { dashboardActions } = this.props
    // regionData
    dashboardActions.fetchRegionData()
    // activeData
    dashboardActions.fetchActiveData()
    // playVVData
    dashboardActions.fetchTimePlayData()
    dashboardActions.fetchPlayUVData()
    // movie/play/edu/app
    dashboardActions.fetchMovieData()
    dashboardActions.fetchPlayData()
    dashboardActions.fetchEduData()
    dashboardActions.fetchAppData()
    // active data
    dashboardActions.fetchMonthActiveData()
    // average data
    dashboardActions.fetchAverageData()
    // totaluser data
    dashboardActions.fetchTotalUserData()
  }

  startRefreshTimer = () => {
    this.refreshTimer = setInterval(() => {
      this.fetchData()
    }, REFRESH_TIMER * 1000)
  }

  componentDidMount () {
    this.fetchData()
    this.startCoinRoll()
    this.startRefreshTimer()

    // error handle
    window.removeEventListener('error', this.fetchData)
  }

  componentWillUnmount () {
    window.removeEventListener('error', this.fetchData)
  }

  componentDidCatch (error, errorInfo) {
    this.fetchData()
  }

  render () {
    return (
      <div className={ clsCreator() }>
        <div className={ clsCreator('hd') }>
          <span className="title">
            环球天成大数据可视化分析
          </span>
        </div>
        <div className={ clsCreator('bd') }>
          <div className='row flex clearfix'>
            { this.renderRegionRankData() }
            { this.renderRegionActiveData() }
            <div className={ clsCreator('play-vv-and-source') }>
              { this.renderPlayVVData() }
              { this.renderSourceData() }
            </div>
          </div>
          <div className='row clearfix' style={{ marginBottom: 0 }}>
            <div className={ clsCreator('month-total-wrapper') }>
              <Coin side={ this.state.coinSide }
                front={ this.renderMonthActiveData() } 
                back={ this.renderTotalUserData() } />
            </div>
            { this.renderPlayUVData() }
            { this.renderAverageData() }
          </div>
        </div>
      </div>
    )
  }

  renderRegionRankData () {
    const { regionData } = this.props
    return (
      <div className={ clsCreator('region-rank-wrapper') }>
        {
          !regionData ? (
            <Loading />
          ) : this.renderRegionRank()
        }
      </div>
    )
  }

  renderRegionRank () {
    const { regionData, regionTotal, activeData } = this.props
    if (!regionData || !regionTotal) {
      return null
    }
    let maxCount = regionData[0].count
    
    return (
      <div className={ clsCreator('region-rank') }>
        <div className={ clsCreator('region-rank-hd') }>
          <p className={ clsCreator('region-rank-title') }>
            用户数地区排名
          </p>
          <p className={ clsCreator('region-rank-number') }>
            { regionTotal }
          </p>
        </div>
        <ol className={ clsCreator('region-rank-list') }>
          {
            regionData.map((item, index) => {
              let style = {
                width: `${item.count/(maxCount * 1.2) * 100}%`,
                backgroundColor: PERCENT_COLOR[index] ? PERCENT_COLOR[index] : 'rgba(255, 255 , 255, .3)'
              }
              return (
                <li key={`_region_rank_${index}`}
                  className={ clsCreator('region-rank-item') }>
                  <span className={ clsCreator('region-rank-index')}>
                    { index + 1 }
                  </span>
                  <div className={ clsCreator('region-rank-data') }>
                    <span className={ clsCreator('region-name') }>
                      { item.province }
                    </span>
                    <div className={ clsCreator('region-percent') }>
                      <span className={ clsCreator('region-percent-inner') }
                        style={ style }></span>
                      <span className={ clsCreator('region-number') }>
                        { `总数:${item.count}万 / 活跃:${item.active}万` }
                      </span>
                    </div>
                  </div>
                </li>
              )
            })
          }
        </ol>
      </div>
    )
  }

  renderRegionActiveData () {
    const { regionData, regionTotal, activeData } = this.props

    return (
      <div className={ clsCreator('region-active-wrapper') }>
        {
          !activeData || !regionData ? (
            <Loading />
          ) : this.renderRegionActiveDataMain()
        }
        
      </div>
    )
  }

  renderRegionActiveDataMain () {
    const { regionData, regionTotal, activeData } = this.props
    return (
      <div className={ clsCreator('region-active') }>
        <div className={ clsCreator('region-active-num') }>
          <div className={ clsCreator('region-active-item') }>
            <p className={ clsCreator('region-active-title') }>实时在线人数</p>
            <p>{ activeData.active }</p>
          </div>
          <div className={ clsCreator('region-active-item') }>
            <p className={ clsCreator('region-active-title') }>今日新增人数</p>
            <p>{ activeData.new }</p>
          </div>
        </div>
        { this.renderRegionMapImage() }
      </div>
    )
  }

  renderRegionMapImage () {
    return (
      <div className={ clsCreator('region-map-image') }>
      </div>
    )
  }

  renderRegionMap () {
    const { regionData } = this.props
    const ds = new DataSet()
    const mapDv = ds.createView('back')
                    .source(GeoJson, { type:'GeoJSON' })
                    .transform({
                        type: 'geo.projection',
                        projection: 'geoMercator',
                        as: [ 'x', 'y', 'centroidX', 'centroidY' ]
                    })

    const userDv = ds.createView('user')
                     .source(regionData)
                     .transform({
                        geoDataView: mapDv,
                        field: 'province',
                        type: 'geo.region',
                        as: [ 'longitude', 'latitude' ]
                     })
    const cols = {
      x: { sync: true, nice: false },
      y: { sync: true, nice: false }
    }
    return (
      <div className={ clsCreator('region-map') }>
        <Chart height={ 625 } data scale={cols} padding={[100, 50, 20, 50]} forceFit>
          <Coord reflect />
          <Legend visible={false} />
          <Axis visible={false} />
          <View data={ mapDv }>
            <Geom type='polygon' tooltip={false} 
              position='x*y' 
              style={{fill: 'transparent',lineWidth: 1,stroke: '#00A2EE',fillOpacity: 0.85}}/>
          </View>
          <View data={ userDv }>
            <Geom type='point' position='x*y' 
                size={['province', [2, 30]]} 
                shape='circle' 
                opacity={ 0.45 } 
                color="#FF2F29" />
          </View>
         </Chart>
      </div>
    )
  }

  renderPlayVVData () {
    const { playVVData, playVVTotal } = this.props
    return (
      <div className={ clsCreator('play-vv-wrapper') }>
        {
          !playVVData ? (
            <Loading />
          ) : (
            <div className={ clsCreator('play-vv') }>
              <div className={ clsCreator('play-vv-title') }>
                <p>
                  分时播放VV(日)<em>单位/万</em>
                </p>
              </div>
              <div className={ clsCreator('play-vv-total') }>
                <p>截止目前</p>
                <p className={ 'total-number' }>
                  { playVVTotal } vv 累计
                </p>
              </div>
              { this.renderPlayVVChart() }
            </div>
          )
        }
      </div>
    )
  }

  renderPlayVVChart () {
    const { playVVData, playVVTotal } = this.props
    let data = playVVData
    const cols = {
      count: { min: 0},
      time: { range: [0, 1] }
    }
    return (
      <div className={ clsCreator('play-vv-chart') }>
        <Chart height={ 260 } width={ 700 } data={ data }  
          padding={[40, 30, 40, 80]} scale={cols} forceFit>
          <Axis name='time' label={{ textStyle: {fill: CHART_TEXT, rotate: -30 }, autoRotate: false}} />
          <Axis name='count' label={{ textStyle: {fill: CHART_TEXT}}}
            grid={{type: 'line', lineStyle: {stroke: CHART_LINE}}} />
          <Tooltip crosshairs={{type : "y"}}/>
          <Geom type="line" position="time*count" 
            size={2} />
          <Geom type="area" position="time*count" />
          <Geom type='point' position="time*count" 
            size={4} shape={'circle'} 
            style={{ stroke: '#fff', lineWidth: 1}} />
        </Chart>
      </div>
    )
  }

  renderSourceData () {
    const { movieData, playData, 
            eduData, appData } = this.props
    
    const { coinSide } = this.state
    
    return (
      <div className={ clsCreator('source-circles') }>
        <div className={ clsCreator('circle-wrap') }>
          {
            movieData && eduData ? (
              <div className={ clsCreator('circle-tags') }>
                <Coin size={ 244 } side={ coinSide }
                  front={ <TagCircle title='电影热播' tags={ movieData } /> } 
                  back={ <TagCircle title='教育资源' tags={ eduData } /> } />
              </div>
            ) : (
              <Loading size={60} />
            )
          }
          
        </div>
        <div className={ clsCreator('circle-wrap') }>
          {
            playData && appData ? (
              <div className={ clsCreator('circle-tags') }>
                <Coin size={ 244 } side={ coinSide }
                  front={ <TagCircle title='剧集热播'tags={  playData } /> } 
                  back={ <TagCircle title='热门应用' tags={ appData } /> } />
              </div>
            ) : (
              <Loading size={60} />
            )
          }
        </div>
      </div>
    )
  }

  renderMonthActiveData () {
    const { mounthMonthActiveData } = this.props
    return (
      <div className={ clsCreator('active-wrapper') }>
        {
          !mounthMonthActiveData ? (
            <Loading />
          ) : (
            <div className={ clsCreator('active') }>
              <div className={ clsCreator('active-title') }>
                <p>
                  用户活跃趋势(月)<em>单位/千</em>
                </p>
              </div>
              { this.renderMonthActiveChart() }
            </div>
          )
        }
      </div>
    )
  }

  renderMonthActiveChart () {
    const { mounthMonthActiveData } = this.props
    let data = mounthMonthActiveData
    const cols = {
      count: { min: 0},
      date: { range: [0, 1] }
    }

    return (
      <div className={ clsCreator('active-chart') }>
        <Chart height={ 350 } width={ 575 } data={ data }  
          padding={[80, 20, 50, 80]} scale={cols} forceFit>
          <Axis name='date' label={{ textStyle: {fill: CHART_TEXT, rotate: -30}, autoRotate: false}} />
          <Axis name='count' label={{ textStyle: {fill: CHART_TEXT}}}
            grid={{type: 'line', lineStyle: {stroke: CHART_LINE}}} />
          <Tooltip crosshairs={{type : "y"}}/>
          <Geom type="line" position="date*count" 
            size={2} />
          <Geom type="area" position="date*count" />
          <Geom type='point' position="date*count" 
            size={4} shape={'circle'} 
            style={{ stroke: '#fff', lineWidth: 1}} />
        </Chart>
      </div>
    )
  }

  renderTotalUserData () {
    const { totalUserData } = this.props
    return (
      <div className={ clsCreator('total-user-wrapper') }>
        {
          !totalUserData ? (
            <Loading />
          ) : (
            <div className={ clsCreator('active') }>
              <div className={ clsCreator('active-title') }>
                <p>
                  累计新增用户(月)<em>单位/千</em>
                </p>
              </div>
              { this.renderTotalUserChart() }
            </div>
          )
        }
      </div>
    )
  }

  renderTotalUserChart () {
    const { totalUserData } = this.props
    let data = totalUserData
    const cols = {
      count: { min: 0},
      date: { range: [0, 1] }
    }

    return (
      <div className={ clsCreator('total-user-chart') }>
        <Chart height={ 350 } width={ 575 } data={ data }  
          padding={[80, 20, 50, 80]} scale={cols} forceFit>
          <Axis name='date' label={{ textStyle: {fill: CHART_TEXT, rotate: -30}, autoRotate: false}} />
          <Axis name='count' label={{ textStyle: {fill: CHART_TEXT}}}
            grid={{type: 'line', lineStyle: {stroke: CHART_LINE}}} />
          <Tooltip crosshairs={{type : "y"}}/>
          <Geom type="line" position="date*count" 
            size={2} />
          <Geom type="area" position="date*count" />
          <Geom type='point' position="date*count" 
            size={4} shape={'circle'} 
            style={{ stroke: '#fff', lineWidth: 1}} />
        </Chart>
      </div>
    )
  }

  renderPlayUVData () {
    const { playUVData, playUVTotal } = this.props

    return (
      <div className={ clsCreator('play-uv-wrapper') }>
        {
          !playUVData ? (
            <Loading />
          ) : (
            <div className={ clsCreator('play-uv') }>
              <div className={ clsCreator('play-uv-title') }>
                <p>
                  分时点播用户(日)<em>单位/千</em>
                </p>
              </div>
              <div className={ clsCreator('play-uv-total') }>
                <p>截止目前</p>
                <p className={ 'total-number' }>
                  { playUVTotal } 人观看
                </p>
              </div>
              { this.renderPlayUVChart() }
            </div>
          )
        }
      </div>
    )
  }

  renderPlayUVChart () {
    const { playUVData } = this.props
    let data = playUVData
    const cols = {
    }
    return (
      <div className={ clsCreator('play-uv-chart') }>
        <Chart height={ 350 } width={ 700 } data={ data }
          padding={[80, 20, 50, 60]} forceFit>
          <Axis name='time' label={{ textStyle: {fill: CHART_TEXT, rotate: -30}, autoRotate: false}} />
          <Axis name='count' label={{ textStyle: {fill: CHART_TEXT}}}
            grid={{type: 'line', lineStyle: {stroke: CHART_LINE}}} />
          <Tooltip crosshairs={{type : "y"}}/>
          <Geom type="interval" position="time*count" />
        </Chart>
      </div>
    )
  }

  renderAverageData () {
    const { averageData } = this.props

    return (
      <div className={ clsCreator('average-wrapper') }>
        {
          !averageData ? (
            <Loading />
          ) :  (
            <div className={ clsCreator('average') }>
              <div className={ clsCreator('average-title') }>
                <p>
                  日均数据
                </p>
              </div>
              { this.renderAverageChart() }
            </div>
          )
        }
      </div>
    )
  }

  renderAverageChart () {
    const { averageData } = this.props
    return (
      <div className={ clsCreator('average-chart') }>
        <div className={ clsCreator('average-chart-item') }>
          <div className={ clsCreator('average-item-main') }>
            <p className='item-title'>
              日均<br/>新增人数
            </p>
            <i className="line"></i>
            <p className="number">{ averageData.new }万</p>
          </div>
          <div className={ clsCreator('average-item-history') }>
            <p className='item-title'>
              历史<br/>平均新增人数
            </p>
            <p className="number">{ averageData.newAvg }万</p>
            <i className="line"></i>
            <p className="percent">{ `${averageData.newPercent}%` }</p>
          </div>
        </div>
        <div className={ clsCreator('average-chart-item') }>
          <div className={ clsCreator('average-item-main') }>
            <p className='item-title'>
              日均<br/>活跃人数
            </p>
            <i className="line"></i>
            <p className="number">{ averageData.active }万</p>
          </div>
          <div className={ clsCreator('average-item-history') }>
            <p className='item-title'>
              历史<br/>平均活跃人数
            </p>
            <p className="number">{ averageData.activeAvg }万</p>
            <i className="line"></i>
            <p className="percent">{ `${averageData.activePercent}%` }</p>
          </div>
        </div>
      </div>
    )
  }
}


// helpers
function mapStateToProps(state) {
  return state
}

function mapDispatchToProps(dispatch) {
  return {
    dashboardActions: bindActionCreators(DashboardActions, dispatch),
  }
}

/**
 * 
 * @param {dates} 日期列表
 * @param {list} 数据列表
 * name: 'xxx',
 * values: []
 */
function compileDataForTrend(dates, list) {
  let result = dates.map((date, index) => {
    let _data = { date }
    list.forEach((item) => {
      let {name, values} = item
      _data[name] = values[index] | ''
    })
    return _data
  })
  return result
}

export default connect(mapStateToProps, mapDispatchToProps)(DashboardView)
