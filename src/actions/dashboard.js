import { RegionDataFetch, ActiveDataFetch, MovieDataFetch,
         PlayDataFetch, EduDataFetch, AppDataFetch,
         TimePlayDataFetch, PlayUVDataFetch,
         MonthActiveDataFetch, AverageDataFetch,
         TotalUserDataFetch } from 'constants/action-types'


export function fetchRegionData() {
  return {
    type: RegionDataFetch.fetch
  }
}

export function fetchActiveData() {
  return {
    type: ActiveDataFetch.fetch
  }
}

export function fetchMovieData() {
  return {
    type: MovieDataFetch.fetch
  }
}

export function fetchPlayData() {
  return {
    type: PlayDataFetch.fetch
  }
}

export function fetchEduData() {
  return {
    type: EduDataFetch.fetch
  }
}

export function fetchAppData() {
  return {
    type: AppDataFetch.fetch
  }
}

export function fetchTimePlayData() {
  return {
    type: TimePlayDataFetch.fetch
  }
}

export function fetchPlayUVData() {
  return {
    type: PlayUVDataFetch.fetch
  }
}

export function fetchMonthActiveData() {
  return {
    type: MonthActiveDataFetch.fetch
  }
}

export function fetchTotalUserData() {
  return {
    type: TotalUserDataFetch.fetch
  }
}

export function fetchAverageData() {
  return {
    type: AverageDataFetch.fetch
  }
}