## 说明


### 可运行文件

最终的可运行文件放在了 `dist`路径下, 需要依次引入

- app.css
- manifest.js
- vendor.js
- app.js

### 本地调试和查看效果

自带一个 webpack dev server, 可以在本地的`8000`端口起个服务查看效果。
按照以下顺序执行命令

```
yarn install

npm run serve
```

会将 api 请求自动转发到 [tvdata-mock](https://gitlab.com/tobeyouth/tvdata-mock) 这个 mock url 上, 如果需要更改配置，可以在

```
/build/config/custom.js
```

中进行修改

### 开发

自带了

```
npm run dev

npm run build
```

两个命令，可以用于开发调试和编译最终可运行文件