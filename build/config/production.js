/**
 * production webpack config
 */
const webpack = require('webpack')
const ExtractTextPlugin = require("extract-text-webpack-plugin")
const constants = require('./const')


let dllManifest = require(constants.dllPath)

module.exports = {
  // webpack configure
  mode: 'production',
  cache: false,
  devtool: 'source-map',
  plugins: [
    new webpack.DllReferencePlugin({
      context: __dirname,
      manifest: dllManifest
    }),
    new ExtractTextPlugin("[name].css"),
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify('production') 
      }
    })
  ]
}