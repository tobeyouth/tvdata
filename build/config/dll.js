const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')
const base = require('./base')
const constants = require('./const')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

let _module = base.module

module.exports = {
  name: 'dll',
  mode: 'production',
  entry: {
    vendor: constants.dll
  },
  output: {
    path: constants.distPath,
    filename: '[name].js',
    library: '[name]'
  },
  module: _module,
  plugins: [
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify('production') 
      }
    }),
    new webpack.DllPlugin({
      context: __dirname,
      path: constants.dllPath,
      name: '[name]'
    }),
    new ExtractTextPlugin("[name].css")
  ]
}