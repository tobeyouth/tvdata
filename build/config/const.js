const path = require('path')
const staticPath = path.resolve(__dirname, './../../')

module.exports = {
  staticPath: staticPath,
  srcPath: path.resolve(staticPath, './src'),
  distPath: path.resolve(staticPath, './dist'),
  appPath: path.resolve(staticPath, './src/app.js'),
  indexPath: path.resolve(staticPath, './src/app.js'),
  serveFilePath: path.resolve(staticPath, './src/index.html'),
  dllPath: path.resolve(staticPath, './dist/manifest.json'),
  dll: [
    'react',
    'prop-types',
    'react-dom',
    'redux',
    'react-redux',
    'redux-saga',
    'redux-logger',
    'regenerator-runtime'
  ],
  devHost: 'localhost',
  devPort: 8000
}

