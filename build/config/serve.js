
/**
 * development server webpack config
 */
const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')
const ExtractTextPlugin = require("extract-text-webpack-plugin")
const AddAssetHtmlPlugin = require('add-asset-html-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const constants = require('./const')


let baseConfig = require('./base')
let customConfig = require('./custom')
let dllManifest = require(constants.dllPath)


let serveConfig = {
  // webpack configure
  mode: 'development',
  cache: true,
  devtool: 'cheap-module-eval-source-map',
  devServer: {
    hot: false
  },
  plugins: [
    new webpack.DllReferencePlugin({
      context: __dirname,
      manifest: dllManifest
    }),
    new ExtractTextPlugin("[name].css"),
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify('dev') 
      }
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new HtmlWebpackPlugin({
      template: constants.serveFilePath,
      chunks: ['manifest', 'app'],
      inject: true
    }),
    new AddAssetHtmlPlugin({
      filepath: path.resolve(constants.distPath, './vendor.js')
    })
  ],
  // dev server
  devServer: {
    clientLogLevel: 'warning',
    hot: true,
    compress: true,
    contentBase: constants.staticPath,
    watchContentBase: true,
    watchOptions: {
      poll: true
    }
  }
}

module.exports = merge({}, baseConfig, customConfig, serveConfig)