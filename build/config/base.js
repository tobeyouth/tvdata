/**
 * base webpack config
 */
const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const autoprefixer = require('autoprefixer')
const cssnano = require('cssnano')
const constants = require('./const')

module.exports = {
  output: {
    path: constants.distPath,
    filename: '[name].js',
    hotUpdateChunkFilename: '.hot/hot-update.js',
    hotUpdateMainFilename: '.hot/hot-update.json'
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json'],
    modules: [
      'node_modules',
      constants.srcPath
    ]
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)?$/,
        type: 'javascript/auto',
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.json$/,
        type: 'javascript/auto',
        exclude: /node_modules/,
        loader: 'json-loader'
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                minimize: true
              }
            }
          ]
        })
      },
      {
        test: /\.(woff|woff2|ttf|TTF|eot)$/,
        loader: 'url-loader',
        options: {
          limit: 200000
        }
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                minimize: true
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                ident: 'postcss',
                plugins: [
                  autoprefixer()
                ]
              }
            },
            {
              loader: 'sass-loader'
            }
          ]
        })
      },
      {
        test: /\.(png|jpe?g|gif)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 2000000
        }
      },
      {
        test: /\.svg$/,
        loader: 'svg-url-loader'
      }
    ]
  },
  optimization: {
    // webpack manifest
    runtimeChunk: {
      name: "manifest"
    },
    splitChunks: false
  },
  context: constants.staticPath,
  target: 'web'
}