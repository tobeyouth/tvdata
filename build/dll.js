const webpack = require('webpack')
const PrettyError = require('pretty-error')
const chalk = require('chalk')

let pe = new PrettyError()
let dllConfig = require('./config/dll')


let startTime = new Date().getTime()

webpack(dllConfig, (err, stats) => {
  if (err) {
    console.log(chalk.bold.red(pe.render(new Error(err))))
  }
  if (stats) {
    let _stats = stats.toString()
    if (_stats.match(/ERROR/g)) {
      console.log(chalk.bold.red(`[webpack] ${_stats}`))
    } else if (_stats.match(/WARNING/g)) {
      // console.log(chalk.bold.yellow(`[webpack] ${_stats}`))
    } else {
      console.log(chalk.cyan(`[webpack] ${_stats}`))
    }
  }
  let endTime = new Date().getTime()
  console.log(chalk.bold.green(`compile dll use: ${endTime - startTime}ms`))
})